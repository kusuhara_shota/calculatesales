package jp.alhinc.kusuhara_shota.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args){

		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> blanchNames = new HashMap<>();
		Map<String, Long> blanchSales = new HashMap<String,Long>();

		BufferedReader br = null;


		try {
			File file = new File(args[0], "branch.lst");

			if(!(file.exists())){
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;


			while ((line = br.readLine()) != null) {
				String[] data = line.split(",");

				if(data.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				if ( !(data[0].matches("^[0-9]{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				blanchNames.put(data[0],data[1]);
				blanchSales.put(data[0],0L);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		ArrayList<String> rcdList = new ArrayList<>();
		File[] files = new File(args[0]).listFiles();

		ArrayList<Integer> rcdNumber = new ArrayList<Integer>();

		for (int i = 0; i < files.length; i++){
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}\\.rcd$")){
				rcdList.add(files[i].getName());
				rcdNumber.add(Integer.parseInt( files[i].getName().substring(0,8) ) );
			}
		}

		for(int i = 1; i < rcdList.size(); i++){
			if(rcdNumber.get(i) != rcdNumber.get(i-1) +1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
	    }

	    for (int i = 0; i < rcdList.size(); i++) {
	    	try{
	    		File file = new File(args[0], rcdList.get(i));
			    FileReader fr = new FileReader(file);
			    br = new BufferedReader(fr);
				ArrayList<String> rcdData = new ArrayList<>();
				String sales;

			    while ((sales = br.readLine()) != null) {
			    	rcdData.add(sales);
			    }

			    if(rcdData.size() != 2){
			    	System.out.println(files[i].getName() + "のフォーマットが不正です");
			    	return;
			    }
			    if(!(rcdData.get(1).matches("^[0-9]+$"))){
			    	System.out.println("予期せぬエラーが発生しました");
			    	return;
			    }
			    if (!(blanchNames.containsKey(rcdData.get(0)))) {
					System.out.println(files[i].getName() + "の支店コードが不正です");
					return;
			    }


			    long branchSalesAmount = 0;

			    branchSalesAmount = blanchSales.get(rcdData.get(0)) + Long.parseLong(rcdData.get(1));
			    if(branchSalesAmount >= 10000000000L){
			    	System.out.println("合計金額が10桁を超えました");
			    	return;
			    }
			    blanchSales.put(rcdData.get(0), branchSalesAmount);


	    	} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
		    }
	    }
	    if(!calculateOutput(args[0], "branch.out", blanchNames, blanchSales)){
	    	return;
	    }

	}





	public static boolean calculateOutput(String path, String fileName, Map<String, String> Names, Map<String,Long> Sales){


		BufferedWriter bw = null;

	    try{
	    	File file = new File(path, "branch.out");
	    	FileWriter fw = new FileWriter(file);
	    	bw = new BufferedWriter(fw);

	    	for(Map.Entry<String, String> entry : Names.entrySet()) {

	    		 bw.write (entry.getKey() + ","+ entry.getValue() + ","+ Sales.get(entry.getKey()));
	    		 bw.newLine();
	    	}


	    	} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally {
				if (bw != null) {
					try {
						bw.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
        return true;
	}
}
